module gitlab.com/herd-project/herd-repo

require (
	cloud.google.com/go v0.49.0
	cloud.google.com/go/firestore v1.1.0
	cloud.google.com/go/storage v1.0.0
	github.com/gavrilaf/errors v0.1.1
	github.com/google/uuid v1.1.1
	github.com/stretchr/testify v1.4.0
	gitlab.com/herd-project/herd-shared v0.0.0-20200220152600-40c212f60763
	google.golang.org/api v0.14.0
	google.golang.org/appengine v1.6.5 // indirect
	google.golang.org/grpc v1.21.1
)

go 1.13
