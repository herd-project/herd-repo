package media

import (
	"context"
	"fmt"
	"io"

	"cloud.google.com/go/storage"
	"github.com/gavrilaf/errors"
)

const (
	publicAvatarURL = "https://storage.googleapis.com/%s/%s"
)

//go:generate mockery -name URLProvider -outpkg mediamocks -output ./mediamocks -dir .
type URLProvider interface {
	GetURLByName(name string) string
}

//go:generate mockery -name Storage -outpkg mediamocks -output ./mediamocks -dir .
type Storage interface {
	URLProvider

	UploadMedia(ctx context.Context, name string, contentType string, r io.Reader) error
	DeleteMedia(ctx context.Context, name string) error
}

///////////////////////////////////////////////////////////////////////////////////

func NewStorage(ctx context.Context, bucketName string) (Storage, error) {
	storage, err := storage.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	bucket := storage.Bucket(bucketName)
	if bucket == nil {
		return nil, errors.NotFoundf("couldn't open bucket: %s", bucketName)
	}

	return &storageImpl{bucket: bucket, name: bucketName}, nil
}

///////////////////////////////////////////////////////////////////////////////////

type storageImpl struct {
	bucket *storage.BucketHandle
	name   string
}

func (stg *storageImpl) GetURLByName(name string) string {
	if len(name) > 0 {
		return fmt.Sprintf(publicAvatarURL, stg.name, name)
	}
	return ""
}

func (stg *storageImpl) UploadMedia(ctx context.Context, name string, contentType string, r io.Reader) error {
	w := stg.bucket.Object(name).NewWriter(ctx)

	// Warning: storage.AllUsers gives public read access to anyone.
	w.ACL = []storage.ACLRule{{Entity: storage.AllUsers, Role: storage.RoleReader}}
	w.ContentType = contentType

	// Entries are immutable, be aggressive about caching (1 day).
	w.CacheControl = "public, max-age=86400"

	if _, err := io.Copy(w, r); err != nil {
		return err
	}

	if err := w.Close(); err != nil {
		return err
	}

	return nil
}

func (stg *storageImpl) DeleteMedia(ctx context.Context, name string) error {
	return stg.bucket.Object(name).Delete(ctx)
}
