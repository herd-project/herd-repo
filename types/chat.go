package types

import "time"

type ChatMessageType int

const (
	ChatMessageTypeText ChatMessageType = iota + 1
	ChatMessageTypeImage
	ChatMessageTypeLocation
)

type IncomingChatMessage struct {
	EventID  string
	SenderID string
	IsBot    bool
	LocalID  string
	Type     ChatMessageType
	Text     string
}

type ChatMessage struct {
	ID        string          `firestore:"id"`
	Timestamp time.Time       `firestore:"timestamp"`
	Type      ChatMessageType `firestore:"message_type"`
	SenderID  string          `firestore:"sender_id"`
	IsBot     bool            `firestore:"is_bot"`
	Text      string          `firestore:"text"`
	URL       string          `firestore:"url"`
}

type ChatPage struct {
	Messages      []ChatMessage
	Attendees     []EventAttendee
	LastTimestamp *time.Time
}
