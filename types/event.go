package types

type Amenities struct {
	Parking   bool   `json:"parking" firestore:"parking"`
	Bathrooms bool   `json:"bathrooms" firestore:"bathrooms"`
	Water     bool   `json:"water" firestore:"water"`
	Comments  string `json:"comments" firestore:"comments"`
}

type EventDetails struct {
	RunType      string    `json:"run_type" firestore:"run_type"`
	AbilityLevel string    `json:"ability_level" firestore:"ability_level"`
	Terrain      string    `json:"terrain" firestore:"terrain"`
	Amenities    Amenities `json:"amenities" firestore:"amenities"`
}

type EventAttendee struct {
	UserID string `json:"user_id"`
	Name   string `json:"name"`
	Avatar string `json:"avatar"`
}

type Event struct {
	ID          string       `firestore:"id"`
	CreatorID   string       `firestore:"creator_id"`
	Name        string       `firestore:"name"`
	Description string       `firestore:"desc,omitempty"`
	Details     EventDetails `firestore:"details"`
	Schedule    Schedule     `firestore:"schedule"`
	Geo         GeoLocation  `firestore:"geo"`
	Closed      bool         `firestore:"closed"`
}

type EventFullInfo struct {
	Event     Event
	Owner     EventAttendee
	Attendees []EventAttendee
}

type UserEvents struct {
	Created []EventFullInfo
	Joined  []EventFullInfo
}

// Event

func (p Event) Validate() error {
	err := p.Geo.Validate()
	if err != nil {
		return err
	}
	return p.Schedule.Validate()
}

// EventAttendee

func NewAttendeeFromProfile(profile UserProfile) EventAttendee {
	return EventAttendee{
		UserID: profile.ID,
		Name:   profile.Name,
		Avatar: profile.Avatar,
	}
}
