package types

import (
	"fmt"
	"math"
	"time"

	"cloud.google.com/go/civil"

	"github.com/gavrilaf/errors"
)

var ErrInvalidAge = errors.NotValidf("age must be over 13")

type UserProfile struct {
	ID           string     `firestore:"id"`
	Name         string     `firestore:"name"`
	DoB          civil.Date `firestore:"dob"`
	Location     string     `firestore:"location"`
	Bio          string     `firestore:"bio"`
	Email        string     `firestore:"email"`
	AveragePace  Pace       `firestore:"average_pace"`
	Interests    string     `firestore:"interests"`
	TrainingFor  string     `firestore:"training_for"`
	Avatar       string     `firestore:"avatar"`
	PushToken    string     `firestore:"push_token"`
	JoinedEvents []string   `firestore:"joined_events"`
}

func (p UserProfile) String() string {
	return fmt.Sprintf("UserProfile(%s, %s, %s)", p.ID, p.Name, p.Location)
}

// TODO: pass current date here
func (p UserProfile) Validate() error {
	now := civil.DateOf(time.Now())
	if math.Abs(float64(p.DoB.DaysSince(now)/365)) <= 13 || math.Abs(float64(p.DoB.DaysSince(now)/365)) >= 150 {
		return ErrInvalidAge
	}
	return nil
}
