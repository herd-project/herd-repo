package types_test

import (
	"testing"

	"cloud.google.com/go/civil"
	"github.com/stretchr/testify/assert"

	"gitlab.com/herd-project/herd-repo/types"
)

func TestTypeEventValidate(t *testing.T) {
	tests := []struct {
		name string
		mdl  types.Event
		want error
	}{
		{
			name: "succeeded",
			mdl: types.Event{
				Schedule: types.Schedule{
					Time:       civil.Time{Hour: 10, Minute: 15},
					StartDate:  civil.Date{Year: 3000, Month: 11, Day: 1},
					Recurrence: "RRULE:FREQ=DAILY",
				},
				Geo: types.GeoLocation{
					Point: types.GeoPoint{
						Latitude:  50.45466,
						Longitude: 30.5238,
					},
				},
			},
			want: nil,
		},
		{
			name: "invalid Latitude",
			mdl: types.Event{
				Schedule: types.Schedule{
					Time:       civil.Time{Hour: 10, Minute: 15},
					StartDate:  civil.Date{Year: 2020, Month: 11, Day: 2},
					Recurrence: "RRULE:FREQ=DAILY"},
				Geo: types.GeoLocation{
					Point: types.GeoPoint{
						Latitude:  90.45466,
						Longitude: 30.5238,
					},
				},
			},
			want: types.ErrInvalidLatitude,
		},
		{
			name: "StartDate is after EndDate",
			mdl: types.Event{
				Schedule: types.Schedule{
					Time:       civil.Time{Hour: 10, Minute: 15},
					StartDate:  civil.Date{Year: 3021, Month: 11, Day: 1},
					EndDate:    civil.Date{Year: 3020, Month: 11, Day: 1},
					Recurrence: "RRULE:FREQ=DAILY"},
				Geo: types.GeoLocation{
					Point: types.GeoPoint{
						Latitude:  50.45466,
						Longitude: 30.5238,
					},
				},
			},
			want: types.ErrInvalidEndDate,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.mdl.Validate()
			assert.Equal(t, err, tt.want)
		})
	}

}
