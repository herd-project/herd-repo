package types

type DistanceUnit int

const (
	Kilos = iota
	Miles
)

type Pace struct {
	Mins  int          `json:"mins" firestore:"mins"`
	Secs  int          `json:"secs" firestore:"secs"`
	Units DistanceUnit `json:"units" firestore:"units"`
}
