package types_test

import (
	"testing"

	"cloud.google.com/go/civil"
	"github.com/stretchr/testify/assert"

	"gitlab.com/herd-project/herd-repo/types"
)

func TestTypeProfileValidate(t *testing.T) {
	tests := []struct {
		name string
		mdl  types.UserProfile
		want error
	}{
		{
			name: "succeeded",
			mdl:  types.UserProfile{DoB: civil.Date{Year: 2000, Month: 4, Day: 16}},
			want: nil,
		},
		{name: "exactly 13",
			mdl:  types.UserProfile{DoB: civil.Date{Year: 2005, Month: 9, Day: 29}},
			want: nil,
		},
		{
			name: "younger than 13",
			mdl:  types.UserProfile{DoB: civil.Date{Year: 2015, Month: 9, Day: 29}},
			want: types.ErrInvalidAge,
		},
		{
			name: "empty age",
			mdl:  types.UserProfile{},
			want: types.ErrInvalidAge,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.mdl.Validate()
			assert.Equal(t, err, tt.want)
		})
	}
}
