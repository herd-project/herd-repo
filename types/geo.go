package types

import (
	"fmt"
	"github.com/gavrilaf/errors"
)

var ErrInvalidLatitude = errors.NotValidf("invalid latitude")
var ErrInvalidLongitude = errors.NotValidf("invalid longitude")

type Address struct {
	Country string `json:"country" firestore:"country"`
	Region  string `json:"region" firestore:"region"`
	City    string `json:"city" firestore:"city"`
	Postal  string `json:"postal" firestore:"postal"`
	Line1   string `json:"line1" firestore:"line1"`
	Line2   string `json:"line2" firestore:"line2"`
}

func (p Address) String() string {
	return fmt.Sprintf("Address(%s, %s, %s, %s, %s, %s)", p.Country, p.Region, p.City, p.Postal, p.Line1, p.Line2)
}

type GeoPoint struct {
	Latitude  float64 `json:"latitude" firestore:"latitude"`
	Longitude float64 `json:"longitude" firestore:"longitude"`
}

func (p GeoPoint) String() string {
	return fmt.Sprintf("Point(%f, %f)", p.Latitude, p.Longitude)
}

type GeoLocation struct {
	Title   string   `json:"title" firestore:"title"`
	Point   GeoPoint `json:"point" firestore:"point"`
	Address Address  `json:"address" firestore:"address"`
}

func (p GeoLocation) String() string {
	return fmt.Sprintf("GeoLocation(%s, %v, %v)", p.Title, p.Point, p.Address)
}

func (p GeoLocation) Validate() error {
	if -90.0 > p.Point.Latitude || p.Point.Latitude > 90.0 {
		return ErrInvalidLatitude
	}
	if -180.0 > p.Point.Longitude || p.Point.Longitude > 180.0 {
		return ErrInvalidLongitude
	}
	return nil
}
