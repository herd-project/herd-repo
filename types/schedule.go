package types

import (
	"cloud.google.com/go/civil"

	"github.com/gavrilaf/errors"
)

var EmptyDate = civil.Date{}

var ErrInvalidEndDate = errors.NotValidf("EndDate must be over than StartTime")

type Schedule struct {
	Time       civil.Time `firestore:"time"`
	StartDate  civil.Date `firestore:"start_date"`
	EndDate    civil.Date `firestore:"end_date"`
	Recurrence string     `firestore:"recurrence"`
}

func (p Schedule) Validate() error {
	if p.EndDate != EmptyDate {
		if p.EndDate.Before(p.StartDate) {
			return ErrInvalidEndDate
		}
	}

	return nil
}
