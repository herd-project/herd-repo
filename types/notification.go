package types

import (
	"fmt"
	"time"
)

type NotificationType int

const (
	NotificationEventCreate NotificationType = iota + 1
	NotificationEventLocationUpdate
	NotificationEventScheduleUpdate
	NotificationEventChatMsg
	NotificationUserJoinEvent
	NotificationUserLeaveEvent
	NotificationUserCloseEvent
)

var ErrUnsupportedNotificationType = fmt.Errorf("unsupported notification")

type Notification struct {
	ID        string
	Type      NotificationType `firestore:"type"`
	EventID   string           `firestore:"event_id"`
	UserID    string           `firestore:"user_id"`
	Timestamp time.Time        `firestore:"timestamp"`
}
