package repo_test

import (
	"context"
	"fmt"
	"testing"

	"cloud.google.com/go/civil"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/herd-project/herd-repo/repo"
	"gitlab.com/herd-project/herd-repo/types"
	"gitlab.com/herd-project/herd-shared/base"
)

func TestNotifications(t *testing.T) {
	ctx := context.Background()

	db, err := createDB(ctx)
	require.NoError(t, err)

	defer db.Close()

	testData := ntfTestPrepare(t, ctx, db)

	users, err := db.ApplyNotification(ctx, types.NotificationUserJoinEvent, testData.ntUserID1, testData.eventID)
	assert.NoError(t, err)
	assert.ElementsMatch(t, []string{testData.creatorID, testData.attendeeID}, users)

	users, err = db.ApplyNotification(ctx, types.NotificationUserLeaveEvent, testData.ntUserID2, testData.eventID)
	assert.NoError(t, err)
	assert.ElementsMatch(t, []string{testData.creatorID, testData.attendeeID}, users)

	// show notifications in order opposite for adding
	expectedNtfs := []types.Notification{
		{Type: types.NotificationUserLeaveEvent, UserID: testData.ntUserID2, EventID: testData.eventID},
		{Type: types.NotificationUserJoinEvent, UserID: testData.ntUserID1, EventID: testData.eventID},
	}

	creatorNtfs, err := db.GetNotifications(ctx, testData.creatorID)
	assert.NoError(t, err)
	assertEqualNotifications(t, expectedNtfs, creatorNtfs)

	attendeeNtfs, err := db.GetNotifications(ctx, testData.attendeeID)
	assert.NoError(t, err)
	assertEqualNotifications(t, expectedNtfs, attendeeNtfs)

	// delete
	err = db.DeleteNotification(ctx, testData.attendeeID, attendeeNtfs[0].ID)

	expectedNtfs = []types.Notification{
		{Type: types.NotificationUserJoinEvent, UserID: testData.ntUserID1, EventID: testData.eventID},
	}

	attendeeNtfs, err = db.GetNotifications(ctx, testData.attendeeID)
	assert.NoError(t, err)
	assertEqualNotifications(t, expectedNtfs, attendeeNtfs)

	ntfTestCleanup(t, ctx, db, testData)
}

// helpers

// compare notifications list ignore timestamp and ID
func assertEqualNotifications(t *testing.T, expected []types.Notification, actual []types.Notification) {
	for indx, p := range actual {
		expected[indx].Timestamp = p.Timestamp

		assert.NotEmpty(t, p.ID)
		expected[indx].ID = p.ID
	}

	assert.Equal(t, expected, actual)
}

// init & cleanup
type ntfTest_ struct {
	creatorID  string
	attendeeID string
	eventID    string

	ntUserID1 string
	ntUserID2 string
}

func ntfTestPrepare(t *testing.T, ctx context.Context, db repo.DB) ntfTest_ {
	idGen := base.UUIDIDGen{}

	// create subject's users
	ntUserID1 := fmt.Sprintf("nt-user-1-%s", idGen.StringID())
	ntUserProfile1 := types.UserProfile{
		ID:   ntUserID1,
		Name: "nt-user-1",
		DoB:  civil.Date{Year: 2000, Month: 1, Day: 1},
	}
	err := db.UpdateProfile(ctx, ntUserProfile1)
	assert.NoError(t, err)

	ntUserID2 := fmt.Sprintf("nt-user-2-%s", idGen.StringID())
	ntUserProfile2 := types.UserProfile{
		ID:   ntUserID2,
		Name: "nt-user-1",
		DoB:  civil.Date{Year: 2000, Month: 1, Day: 1},
	}
	err = db.UpdateProfile(ctx, ntUserProfile2)
	assert.NoError(t, err)

	// create owner & attendee
	creatorID := fmt.Sprintf("test-notifications-creator-%s", idGen.StringID())
	creatorProfile := types.UserProfile{
		ID:   creatorID,
		Name: "test-profile-creator",
		DoB:  civil.Date{Year: 2000, Month: 1, Day: 1},
	}
	err = db.UpdateProfile(ctx, creatorProfile)
	assert.NoError(t, err)

	attendeeID := fmt.Sprintf("test-notifications-attendee-%s", idGen.StringID())
	attendeeProfile := types.UserProfile{
		ID:   attendeeID,
		Name: "test-profile-attendee",
		DoB:  civil.Date{Year: 2000, Month: 1, Day: 1},
	}
	err = db.UpdateProfile(ctx, attendeeProfile)
	assert.NoError(t, err)

	// create event
	event, err := db.CreateEvent(ctx, types.Event{
		CreatorID: creatorID,
		Name:      "test-event-name",
		Closed:    false,
	})
	assert.NoError(t, err)

	// join group
	err = db.AddToEventAttendees(ctx, attendeeID, event.ID)
	assert.NoError(t, err)

	return ntfTest_{
		creatorID:  creatorID,
		attendeeID: attendeeID,
		eventID:    event.ID,
		ntUserID1:  ntUserID1,
		ntUserID2:  ntUserID2,
	}
}

func ntfTestCleanup(t *testing.T, ctx context.Context, db repo.DB, data ntfTest_) {
	err := db.RemoveFromEventAttendees(ctx, data.attendeeID, data.eventID)
	assert.NoError(t, err)

	err = db.DeleteEvent(ctx, data.eventID)
	assert.NoError(t, err)

	// delete profiles
	err = db.DeleteProfile(ctx, data.creatorID)
	assert.NoError(t, err)

	err = db.DeleteProfile(ctx, data.attendeeID)
	assert.NoError(t, err)

	_ = db.DeleteProfile(ctx, data.ntUserID1)
	_ = db.DeleteProfile(ctx, data.ntUserID2)
}
