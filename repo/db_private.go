package repo

import (
	"context"

	"cloud.google.com/go/firestore"

	"gitlab.com/herd-project/herd-repo/types"
	"gitlab.com/herd-project/herd-shared/herdlog"
)

func (db *dbImpl) getAttendeeWithCache(ctx context.Context, userID string) (types.EventAttendee, error) {
	cacheKey := "a-" + userID
	cached, found := db.cache.Get(ctx, cacheKey)
	if found {
		herdlog.FromContext(ctx).Info("read attendee from cache")
		return cached.(types.EventAttendee), nil
	}

	user, err := db.GetProfile(ctx, userID)
	if err != nil {
		return types.EventAttendee{}, err
	}

	attendee := types.NewAttendeeFromProfile(user)
	db.cache.Set(ctx, cacheKey, attendee)

	return attendee, nil
}

func (db *dbImpl) getEventsByIds(ctx context.Context, ids []string) ([]types.EventFullInfo, error) {
	if len(ids) == 0 {
		return []types.EventFullInfo{}, nil
	}

	eventsRef, err := db.events.Where("id", "in", ids).Documents(ctx).GetAll()
	if err != nil {
		return nil, err
	}

	events := make([]types.EventFullInfo, len(eventsRef))
	for indx, doc := range eventsRef {
		var event types.Event
		err = doc.DataTo(&event)
		if err != nil {
			return nil, err
		}

		owner, err := db.getAttendeeWithCache(ctx, event.CreatorID)
		if err != nil {
			return nil, err
		}

		attendees, err := db.getAttendeesFromSnapshot(ctx, doc)
		if err != nil {
			return nil, err
		}

		events[indx] = types.EventFullInfo{
			Event:     event,
			Owner:     owner,
			Attendees: attendees,
		}
	}

	return events, nil
}

func (db *dbImpl) getAttendeesFromSnapshot(ctx context.Context, doc *firestore.DocumentSnapshot) ([]types.EventAttendee, error) {
	ids := idsArrayFromDoc(doc, "attendees")

	attendees := make([]types.EventAttendee, len(ids))
	for indx, id := range ids {
		attendee, err := db.getAttendeeWithCache(ctx, id)
		if err != nil {
			return nil, err
		}
		attendees[indx] = attendee
	}

	return attendees, nil
}

