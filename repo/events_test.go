package repo_test

import (
	"context"
	"fmt"
	"testing"

	"cloud.google.com/go/civil"
	"github.com/gavrilaf/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/herd-project/herd-repo/types"
	"gitlab.com/herd-project/herd-shared/base"
)

func TestDBEvents(t *testing.T) {
	idGen := base.UUIDIDGen{}
	ctx := context.Background()

	db, err := createDB(ctx)
	require.NoError(t, err)

	defer db.Close()

	// create owner & attendee
	creatorID := fmt.Sprintf("test-events-creator-%s", idGen.StringID())
	creatorProfile := types.UserProfile{
		ID:     creatorID,
		Name:   "test-profile-creator",
		DoB:    civil.Date{Year: 2000, Month: 1, Day: 1},
		Avatar: "creator.png",
	}
	err = db.UpdateProfile(ctx, creatorProfile)
	assert.NoError(t, err)

	err = db.UpdateAvatar(ctx, creatorID, "creator.png")
	assert.NoError(t, err)

	attendeeID := fmt.Sprintf("test-events-attendee-%s", idGen.StringID())
	attendeeProfile := types.UserProfile{
		ID:     attendeeID,
		Name:   "test-profile-attendee",
		DoB:    civil.Date{Year: 2000, Month: 1, Day: 1},
		Avatar: "attendee.png",
	}
	err = db.UpdateProfile(ctx, attendeeProfile)
	assert.NoError(t, err)

	err = db.UpdateAvatar(ctx, attendeeID, "attendee.png")
	assert.NoError(t, err)

	// basic operations

	// create event
	event := types.Event{
		CreatorID: creatorID,
		Name:      "test-event-name",
		Closed:    false,
	}

	createdEvent, err := db.CreateEvent(ctx, event)
	assert.NoError(t, err)
	assert.NotEmpty(t, createdEvent.ID)

	event.ID = createdEvent.ID
	assert.Equal(t, event, createdEvent)

	// join/left, get user groups, get attendees

	// join group
	err = db.AddToEventAttendees(ctx, attendeeID, event.ID)
	assert.NoError(t, err)

	// event has attendees
	attendees, err := db.GetEventAttendees(ctx, event.ID)
	assert.NoError(t, err)

	expectedAttendees := []types.EventAttendee{types.NewAttendeeFromProfile(attendeeProfile)}
	assert.Equal(t, expectedAttendees, attendees)

	// update event
	event.Details = types.EventDetails{
		RunType:      "run-type",
		AbilityLevel: "all-level",
		Terrain:      "terrain",
		Amenities:    types.Amenities{},
	}

	err = db.UpdateEvent(ctx, event)
	assert.NoError(t, err)

	got, err := db.GetEvent(ctx, event.ID)
	assert.NoError(t, err)

	assert.Equal(t, event, got)

	// update doesn't change attendees
	attendees, err = db.GetEventAttendees(ctx, event.ID)
	assert.NoError(t, err)
	assert.Equal(t, expectedAttendees, attendees)

	// event full info -> has attendees
	fullInfo, err := db.GetEventFullInfo(ctx, event.ID)
	assert.NoError(t, err)

	assert.Equal(t, event, fullInfo.Event)
	assert.Equal(t, types.NewAttendeeFromProfile(creatorProfile), fullInfo.Owner)
	assert.Equal(t, []types.EventAttendee{types.NewAttendeeFromProfile(attendeeProfile)}, fullInfo.Attendees)

	// for creator
	userEvents, err := db.GetUserEvents(ctx, creatorID)
	assert.NoError(t, err)

	// event in the 'created' list, empty 'joined' list
	assert.Equal(t, []types.EventFullInfo{
		{
			Event:     event,
			Owner:     types.NewAttendeeFromProfile(creatorProfile),
			Attendees: expectedAttendees,
		}},
		userEvents.Created)
	assert.Empty(t, userEvents.Joined)

	// for attendee
	userEvents, err = db.GetUserEvents(ctx, attendeeID)
	assert.NoError(t, err)

	assert.Empty(t, userEvents.Created)
	assert.Equal(t, []types.EventFullInfo{fullInfo}, userEvents.Joined)

	// left group
	err = db.RemoveFromEventAttendees(ctx, attendeeID, event.ID)
	assert.NoError(t, err)

	// no attendees
	attendees, err = db.GetEventAttendees(ctx, event.ID)
	assert.NoError(t, err)

	expectedAttendees = []types.EventAttendee{}
	assert.Equal(t, expectedAttendees, attendees)

	fullInfo, err = db.GetEventFullInfo(ctx, event.ID)
	assert.NoError(t, err)

	assert.Equal(t, event, fullInfo.Event)
	assert.Equal(t, types.NewAttendeeFromProfile(creatorProfile), fullInfo.Owner)
	assert.Empty(t, fullInfo.Attendees)

	// user doesn't have any events
	userEvents, err = db.GetUserEvents(ctx, attendeeID)
	assert.NoError(t, err)

	assert.Empty(t, userEvents.Created)
	assert.Empty(t, userEvents.Joined)

	// gallery
	//err = db.AddImageToEventGallery(ctx, createdEvent.ID, "test-image-name")
	//assert.NoError(t, err)

	//err = db.RemoveImageFromEventGallery(ctx, createdEvent.ID, "test-image-name")
	//assert.NoError(t, err)

	// delete event
	err = db.DeleteEvent(ctx, createdEvent.ID)
	assert.NoError(t, err)

	_, err = db.GetEvent(ctx, event.ID)
	assert.True(t, errors.IsNotFound(err))

	// delete profiles
	err = db.DeleteProfile(ctx, creatorID)
	assert.NoError(t, err)

	err = db.DeleteProfile(ctx, attendeeID)
	assert.NoError(t, err)
}
