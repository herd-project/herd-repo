package repo_test

import (
	"context"
	"fmt"
	"testing"

	"cloud.google.com/go/civil"
	"github.com/gavrilaf/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/herd-project/herd-repo/types"
	"gitlab.com/herd-project/herd-shared/base"
)

func TestDBProfile(t *testing.T) {
	idGen := base.UUIDIDGen{}
	ctx := context.Background()

	db, err := createDB(ctx)
	require.NoError(t, err)

	defer db.Close()

	newProfileID := fmt.Sprintf("test-profile-%s", idGen.StringID())

	// return `not found` error if profile doesn't exist
	_, err = db.GetProfile(ctx, newProfileID)
	assert.True(t, errors.IsNotFound(err))

	expected := types.UserProfile{
		ID:   newProfileID,
		Name: "test-profile-name",
		DoB:  civil.Date{Year: 2000, Month: 1, Day: 1},
	}

	// profile is created, Avatar & PushToken fields are empty
	err = db.UpdateProfile(ctx, expected)
	assert.NoError(t, err)

	profile, err := db.GetProfile(ctx, newProfileID)
	assert.NoError(t, err)

	assert.Equal(t, expected, profile)
	assert.Empty(t, profile.Avatar)
	assert.Empty(t, profile.PushToken)

	// set Avatar & PushToken
	err = db.SetPushNotificationsToken(ctx, newProfileID, "push-token")
	assert.NoError(t, err)

	err = db.UpdateAvatar(ctx, newProfileID, "avatar")
	assert.NoError(t, err)

	expected.Avatar = "avatar"
	expected.PushToken = "push-token"

	profile, err = db.GetProfile(ctx, newProfileID)
	assert.NoError(t, err)
	assert.Equal(t, expected, profile)

	// update all fields
	expected.Location = "location"
	expected.TrainingFor = "training for"
	expected.Interests = "interests"
	expected.AveragePace = types.Pace{
		Mins:  5,
		Secs:  5,
		Units: types.Kilos,
	}
	expected.Bio = "bio"
	expected.Email = "test$test.com"
	
	err = db.UpdateProfile(ctx, expected)

	profile, err = db.GetProfile(ctx, newProfileID)
	assert.NoError(t, err)
	assert.Equal(t, expected, profile)

	// Update should not change Avatar & PushToken
	expected.Avatar = "new-avatar"
	expected.PushToken = "new-push-token"

	err = db.UpdateProfile(ctx, expected)

	profile, err = db.GetProfile(ctx, newProfileID)
	assert.NoError(t, err)

	expected.Avatar = "avatar"
	expected.PushToken = "push-token"
	assert.Equal(t, expected, profile)

	// delete profile
	err = db.DeleteProfile(ctx, newProfileID)
	assert.NoError(t, err)

	_, err = db.GetProfile(ctx, newProfileID)
	assert.True(t, errors.IsNotFound(err))
}
