package repo_test

import (
	"context"
	"os"

	"gitlab.com/herd-project/herd-repo/repo"
)

// helper for all test

func createDB(ctx context.Context) (repo.DB, error) {
	projectID := os.Getenv("GCP_PROJECT_ID")
	return repo.NewDB(ctx, projectID, true)
}
