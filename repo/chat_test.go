package repo_test

import (
	"context"
	"fmt"
	"testing"

	"cloud.google.com/go/civil"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/herd-project/herd-repo/types"
	"gitlab.com/herd-project/herd-shared/base"
)

func TestDBEventChat(t *testing.T) {
	idGen := base.UUIDIDGen{}
	ctx := context.Background()

	db, err := createDB(ctx)
	require.NoError(t, err)

	defer db.Close()

	// preparation

	// create owner & attendee
	creatorID := fmt.Sprintf("test-chat-creator-%s", idGen.StringID())
	creatorProfile := types.UserProfile{
		ID:   creatorID,
		Name: "test-chat-creator",
		DoB:  civil.Date{Year: 2000, Month: 1, Day: 1},
		Avatar: "creator.png",
	}
	err = db.UpdateProfile(ctx, creatorProfile)
	assert.NoError(t, err)

	err = db.UpdateAvatar(ctx, creatorID, "creator.png")
	assert.NoError(t, err)

	attendeeID := fmt.Sprintf("test-cat-attendee-%s", idGen.StringID())
	attendeeProfile := types.UserProfile{
		ID:     attendeeID,
		Name:   "test-chat-attendee",
		DoB:    civil.Date{Year: 2000, Month: 1, Day: 1},
		Avatar: "attendee.png",
	}
	err = db.UpdateProfile(ctx, attendeeProfile)
	assert.NoError(t, err)

	err = db.UpdateAvatar(ctx, attendeeID, "attendee.png")
	assert.NoError(t, err)

	// create event
	event := types.Event{
		CreatorID: creatorID,
		Name:      "test-chat-event-name",
		Closed:    false,
	}

	createdEvent, err := db.CreateEvent(ctx, event)
	assert.NoError(t, err)
	assert.NotEmpty(t, createdEvent.ID)

	eventID := createdEvent.ID

	// join event
	err = db.AddToEventAttendees(ctx, attendeeID, createdEvent.ID)
	assert.NoError(t, err)

	assertAddMessage:= func (senderID string, s string) {
		err := db.ChatAddMessage(ctx, types.IncomingChatMessage{
			EventID:  eventID,
			SenderID: senderID,
			IsBot:    false,
			LocalID:  fmt.Sprintf("id-%s", s),
			Type:     types.ChatMessageTypeText,
			Text:     s,
		})
		assert.NoError(t, err)
	}

	// no messages
	page, err := db.ChatGetMessages(ctx, eventID, nil, 2)
	assert.NoError(t, err)

	assert.Empty(t, page.Attendees)
	assert.Empty(t, page.Messages)
	assert.Nil(t, page.LastTimestamp)

	// add some messages
	assertAddMessage(creatorID, "1")
	assertAddMessage(attendeeID, "2")
	assertAddMessage(attendeeID, "3")
	assertAddMessage(attendeeID, "4")
	assertAddMessage(creatorID, "5")
	assertAddMessage(creatorID, "6")

	page, err = db.ChatGetMessages(ctx, eventID, nil, 2)
	assert.NoError(t, err)
	assertChatMessages(t, []string{"1", "2"}, page)
	assertChatAttendees(t, []types.UserProfile{creatorProfile, attendeeProfile}, page)

	page, err = db.ChatGetMessages(ctx, eventID, page.LastTimestamp, 3)
	assert.NoError(t, err)
	assertChatMessages(t, []string{"3", "4", "5"}, page)
	assertChatAttendees(t, []types.UserProfile{creatorProfile, attendeeProfile}, page)

	page, err = db.ChatGetMessages(ctx, createdEvent.ID, page.LastTimestamp, 100)
	assert.NoError(t, err)
	assertChatMessages(t, []string{"6"}, page)
	assertChatAttendees(t, []types.UserProfile{creatorProfile}, page)

	// clean up

	err = db.DeleteEvent(ctx, createdEvent.ID)
	assert.NoError(t, err)

	err = db.DeleteProfile(ctx, attendeeID)
	assert.NoError(t, err)

	err = db.DeleteProfile(ctx, creatorID)
	assert.NoError(t, err)
}


func assertChatMessages(t *testing.T, expected []string, page types.ChatPage) {
	assert.Len(t, page.Messages, len(expected))

	for i, m := range  page.Messages {
		assert.False(t, m.IsBot)
		assert.Equal(t, types.ChatMessageTypeText, m.Type)
		assert.Equal(t, expected[i], m.Text)
		assert.Equal(t, "id-" + expected[i], m.ID)
	}
}

func assertChatAttendees(t *testing.T, expected []types.UserProfile, page types.ChatPage) {
	expectedAttendees := make([]types.EventAttendee, len(expected))
	for i, p := range expected {
		expectedAttendees[i] = types.NewAttendeeFromProfile(p)
	}

	assert.ElementsMatch(t, page.Attendees, expectedAttendees)
}