package repo

import (
	"context"
	"time"

	"cloud.google.com/go/firestore"

	"gitlab.com/herd-project/herd-repo/types"
)

func (db *dbImpl) ChatAddMessage(ctx context.Context, msg types.IncomingChatMessage) error {
	msgMap := map[string]interface{}{
		"id":           msg.LocalID,
		"timestamp":    firestore.ServerTimestamp,
		"message_type": types.ChatMessageTypeText,
		"sender_id":    msg.SenderID,
		"is_bot":       msg.IsBot,
		"text":         msg.Text,
	}

	chat := db.events.Doc(msg.EventID).Collection(subChat)
	doc := chat.NewDoc()

	_, err := doc.Set(ctx, msgMap)
	return err
}

func (db *dbImpl) ChatGetMessages(ctx context.Context, eventID string, startFrom *time.Time, limit int) (types.ChatPage, error) {
	chat := db.events.Doc(eventID).Collection(subChat)

	page := chat.OrderBy("timestamp", firestore.Asc).Limit(limit)
	if startFrom != nil {
		page = page.StartAfter(*startFrom)
	}

	docs, err := page.Documents(ctx).GetAll()
	if err != nil {
		return types.ChatPage{}, err
	}

	messages := make([]types.ChatMessage, len(docs))
	attendeesIds := make(map[string]struct{})

	for indx, doc := range docs {
		var msg types.ChatMessage
		err := doc.DataTo(&msg)
		if err != nil {
			return types.ChatPage{}, err
		}
		messages[indx] = msg
		if !msg.IsBot && msg.SenderID != "" {
			attendeesIds[msg.SenderID] = struct{}{}
		}
	}

	attendees := make([]types.EventAttendee, 0, len(attendeesIds))

	for id := range attendeesIds {
		attendee, err := db.getAttendeeWithCache(ctx, id)
		if err != nil {
			return types.ChatPage{}, err
		}
		attendees = append(attendees, attendee)
	}

	var lastTimestamp *time.Time

	if len(messages) > 0 {
		lastTimestamp = &messages[len(messages)-1].Timestamp
	}

	result := types.ChatPage{
		Messages:      messages,
		Attendees:     attendees,
		LastTimestamp: lastTimestamp,
	}

	return result, nil
}
