package repo

import (
	"context"
	"io"
	"time"

	"gitlab.com/herd-project/herd-shared/base"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/option"

	"gitlab.com/herd-project/herd-repo/types"
	"gitlab.com/herd-project/herd-shared/cache"
)

type DB interface {
	io.Closer

	// profile
	GetProfile(ctx context.Context, userID string) (types.UserProfile, error)
	UpdateProfile(ctx context.Context, profile types.UserProfile) error
	SetPushNotificationsToken(ctx context.Context, userID string, token string) error
	UpdateAvatar(ctx context.Context, userID string, avatar string) error
	DeleteProfile(ctx context.Context, userID string) error

	// event
	GetEvent(ctx context.Context, eventID string) (types.Event, error)
	CreateEvent(ctx context.Context, event types.Event) (types.Event, error)
	UpdateEvent(ctx context.Context, event types.Event) error
	DeleteEvent(ctx context.Context, eventID string) error

	AddToEventAttendees(ctx context.Context, userID string, eventID string) error
	RemoveFromEventAttendees(ctx context.Context, userID string, eventID string) error

	AddImageToEventGallery(ctx context.Context, eventID string, imageName string) error
	RemoveImageFromEventGallery(ctx context.Context, eventID string, imageName string) error

	GetEventAttendees(ctx context.Context, eventID string) ([]types.EventAttendee, error)
	GetEventFullInfo(ctx context.Context, eventID string) (types.EventFullInfo, error)

	GetUserEvents(ctx context.Context, userID string) (types.UserEvents, error)

	// notifications

	// Notification `nType` adds to all users affected by notification
	// Return ids list of the affected users
	ApplyNotification(ctx context.Context, nType types.NotificationType, userID string, eventID string) ([]string, error)
	GetNotifications(ctx context.Context, userID string) ([]types.Notification, error)
	DeleteNotification(ctx context.Context, userID string, ID string) error

	// chat

	ChatAddMessage(ctx context.Context, msg types.IncomingChatMessage) error
	ChatGetMessages(ctx context.Context, eventID string, startFrom *time.Time, limit int) (types.ChatPage, error)
}

// Factory

func NewDB(ctx context.Context, projectID string, testMode bool, opts ...option.ClientOption) (DB, error) {
	fs, err := firestore.NewClient(ctx, projectID, opts...)
	if err != nil {
		return nil, err
	}

	// 1000 entities in the cache with 10s expiration
	cache := cache.New(1000, 10)

	if testMode {
		return &dbImpl{
			fs:     fs,
			events: fs.Collection(eventsTestCollection),
			users:  fs.Collection(usersTestCollection),
			cache:  cache,
			idGen:  base.UUIDIDGen{},
		}, nil
	} else {
		return &dbImpl{
			fs:     fs,
			events: fs.Collection(eventsProdCollection),
			users:  fs.Collection(usersProdCollection),
			cache:  cache,
			idGen:  base.UUIDIDGen{},
		}, nil
	}
}

// impl

type dbImpl struct {
	fs     *firestore.Client
	events *firestore.CollectionRef
	users  *firestore.CollectionRef
	cache  cache.Cache
	idGen  base.IDGen
}

const (
	eventsProdCollection = "Events"
	eventsTestCollection = "TestEvents"

	usersProdCollection = "Users"
	usersTestCollection = "TestUsers"
)

func (db *dbImpl) Close() error {
	if db != nil && db.fs != nil {
		return db.fs.Close()
	}
	return nil
}

// helpers
