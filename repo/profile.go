package repo

import (
	"context"

	"cloud.google.com/go/firestore"
	"github.com/gavrilaf/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/herd-project/herd-repo/types"
	"gitlab.com/herd-project/herd-shared/herdlog"
)

const (
	avatarKey    = "avatar"
	pushTokenKey = "push_token"
)

func (db *dbImpl) GetProfile(ctx context.Context, userID string) (types.UserProfile, error) {
	t := herdlog.NewTrace(ctx, "db.GetProfile")
	defer t.Done()

	dsnap, err := db.users.Doc(userID).Get(ctx)
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return types.UserProfile{}, errors.NotFoundf("profile not found, userID=%s", userID)
		} else {
			return types.UserProfile{}, err
		}
	}

	var mdl types.UserProfile
	err = dsnap.DataTo(&mdl)

	avatar, _ := dsnap.DataAt(avatarKey)
	mdl.Avatar, _ = avatar.(string)

	pushToken, _ := dsnap.DataAt(pushTokenKey)
	mdl.PushToken, _ = pushToken.(string)

	return mdl, err
}

func (db *dbImpl) UpdateProfile(ctx context.Context, profile types.UserProfile) error {
	profileMap := map[string]interface{}{
		"id":           profile.ID,
		"name":         profile.Name,
		"dob":          profile.DoB,
		"email":        profile.Email,
		"location":     profile.Location,
		"bio":          profile.Bio,
		"average_pace": profile.AveragePace,
		"interests":    profile.Interests,
		"training_for": profile.TrainingFor,
	}

	_, err := db.users.Doc(profile.ID).Set(ctx, profileMap, firestore.MergeAll)
	return err
}

func (db *dbImpl) DeleteProfile(ctx context.Context, userID string) error {
	err := deleteCollection(ctx, db.fs, db.users.Doc(userID).Collection(subNotifications), 100)
	if err != nil {
		return err
	}

	_, err = db.users.Doc(userID).Delete(ctx)

	return err
}

func (db *dbImpl) SetPushNotificationsToken(ctx context.Context, userID string, token string) error {
	_, err := db.users.Doc(userID).Set(ctx, map[string]interface{}{pushTokenKey: token}, firestore.MergeAll)
	return err
}

func (db *dbImpl) UpdateAvatar(ctx context.Context, userID string, avatar string) error {
	_, err := db.users.Doc(userID).Set(ctx, map[string]interface{}{avatarKey: avatar}, firestore.MergeAll)
	return err
}
