package repo

import (
	"context"

	"cloud.google.com/go/firestore"
	"github.com/gavrilaf/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/herd-project/herd-repo/types"
	"gitlab.com/herd-project/herd-shared/herdlog"
)

func (db *dbImpl) AddImageToEventGallery(ctx context.Context, eventID string, imageName string) error {
	return errors.NotImplementedf("")
}

func (db *dbImpl) RemoveImageFromEventGallery(ctx context.Context, eventID string, imageName string) error {
	return errors.NotImplementedf("")
}

func (db *dbImpl) GetEvent(ctx context.Context, id string) (types.Event, error) {
	dsnap, err := db.events.Doc(id).Get(ctx)
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return types.Event{}, errors.NotFoundf("event not found, id = %s", id)
		} else {
			return types.Event{}, err
		}
	}

	var mdl types.Event
	err = dsnap.DataTo(&mdl)
	if err != nil {
		return types.Event{}, err
	}

	return mdl, nil
}

func (db *dbImpl) CreateEvent(ctx context.Context, event types.Event) (types.Event, error) {
	ref := db.events.NewDoc()
	event.ID = ref.ID
	if _, err := ref.Set(ctx, event); err != nil {
		return types.Event{}, err
	}
	return event, nil
}

func (db *dbImpl) UpdateEvent(ctx context.Context, event types.Event) error {
	fieldsMap := map[string]interface{}{
		"name":     event.Name,
		"desc":     event.Description,
		"details":  event.Details,
		"schedule": event.Schedule,
		"geo":      event.Geo,
		"closed":	event.Closed,
	}

	_, err := db.events.Doc(event.ID).Set(context.Background(), fieldsMap, firestore.MergeAll)
	return err
}

func (db *dbImpl) DeleteEvent(ctx context.Context, id string) error {
	err := deleteCollection(ctx, db.fs, db.events.Doc(id).Collection(subChat), 100)
	if err != nil {
		return err
	}

	_, err = db.events.Doc(id).Delete(ctx)
	return err
}

func (db *dbImpl) AddToEventAttendees(ctx context.Context, userID string, eventID string) error {
	return db.fs.RunTransaction(ctx, func(ctx context.Context, tx *firestore.Transaction) error {
		err := tx.Update(db.events.Doc(eventID), []firestore.Update{
			{Path: "attendees", Value: firestore.ArrayUnion(userID)},
		})
		if err != nil {
			return err
		}

		return tx.Update(db.users.Doc(userID), []firestore.Update{
			{Path: "joined_events", Value: firestore.ArrayUnion(eventID)},
		})
	})
}

func (db *dbImpl) RemoveFromEventAttendees(ctx context.Context, userID string, eventID string) error {
	return db.fs.RunTransaction(ctx, func(ctx context.Context, tx *firestore.Transaction) error {
		err := tx.Update(db.events.Doc(eventID), []firestore.Update{
			{Path: "attendees", Value: firestore.ArrayRemove(userID)},
		})
		if err != nil {
			return err
		}

		return tx.Update(db.users.Doc(userID), []firestore.Update{
			{Path: "joined_events", Value: firestore.ArrayRemove(eventID)},
		})
	})
}

func (db *dbImpl) GetEventAttendees(ctx context.Context, eventID string) ([]types.EventAttendee, error) {
	doc, err := db.events.Doc(eventID).Get(ctx)
	if err != nil {
		return nil, err
	}

	return db.getAttendeesFromSnapshot(ctx, doc)
}

func (db *dbImpl) GetEventFullInfo(ctx context.Context, eventID string) (types.EventFullInfo, error) {
	t := herdlog.NewTrace(ctx, "db.GetEventFullInfo")
	defer t.Done()

	doc, err := db.events.Doc(eventID).Get(ctx)
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return types.EventFullInfo{}, errors.NotFoundf("event not found, id = %s", eventID)
		} else {
			return types.EventFullInfo{}, err
		}
	}

	var event types.Event
	err = doc.DataTo(&event)
	if err != nil {
		return types.EventFullInfo{}, err
	}

	owner, err := db.getAttendeeWithCache(ctx, event.CreatorID)
	if err != nil {
		return types.EventFullInfo{}, err
	}

	attendees, err := db.getAttendeesFromSnapshot(ctx, doc)
	if err != nil {
		return types.EventFullInfo{}, err
	}

	return types.EventFullInfo{
		Event:     event,
		Owner:     owner,
		Attendees: attendees,
	}, nil
}

func (db *dbImpl) GetUserEvents(ctx context.Context, userID string) (types.UserEvents, error) {
	t := herdlog.NewTrace(ctx, "db.GetUserEvents")
	defer t.Done()

	// created events
	createdEventsRef, err := db.events.Where("creator_id", "==", userID).Documents(ctx).GetAll()
	if err != nil {
		return types.UserEvents{}, err
	}

	owner, err := db.getAttendeeWithCache(ctx, userID)
	if err != nil {
		return types.UserEvents{}, err
	}

	createdEvents := make([]types.EventFullInfo, len(createdEventsRef))
	for indx, createdDoc := range createdEventsRef {
		var event types.Event
		err = createdDoc.DataTo(&event)
		if err != nil {
			return types.UserEvents{}, err // TODO: log or error?
		}

		attendees, err := db.getAttendeesFromSnapshot(ctx, createdDoc)
		if err != nil {
			return types.UserEvents{}, err // TODO: log or error?
		}

		createdEvents[indx] = types.EventFullInfo{
			Event:     event,
			Owner:     owner,
			Attendees: attendees,
		}
	}

	// joined events
	userDoc, err := db.users.Doc(userID).Get(ctx)
	if err != nil {
		return types.UserEvents{}, err
	}

	joinedIds := idsArrayFromDoc(userDoc, "joined_events")
	joinedEvents, err := db.getEventsByIds(ctx, joinedIds)
	if err != nil {
		return types.UserEvents{}, err
	}

	return types.UserEvents{
		Created: createdEvents,
		Joined:  joinedEvents,
	}, nil
}

// helpers
