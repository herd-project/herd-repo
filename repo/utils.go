package repo

import (
	"context"
	"fmt"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/iterator"

	"gitlab.com/herd-project/herd-shared/herdlog"
)

const (
	subNotifications = "notifications"
	subChat          = "chat"
)

func deleteCollection(ctx context.Context, client *firestore.Client, ref *firestore.CollectionRef, batchSize int) error {
	t := herdlog.NewTrace(ctx, fmt.Sprintf("db.deleteCollection: %s", ref.ID))
	defer t.Done()

	for {
		// Get a batch of documents
		iter := ref.Limit(batchSize).Documents(ctx)
		numDeleted := 0

		// Iterate through the documents, adding
		// a delete operation for each one to a
		// WriteBatch.
		batch := client.Batch()
		for {
			doc, err := iter.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return err
			}

			batch.Delete(doc.Ref)
			numDeleted++
		}

		// If there are no documents to delete,
		// the process is over.
		if numDeleted == 0 {
			return nil
		}

		herdlog.FromContext(ctx).Infof("delete-collection, deleting %d recors", numDeleted)
		_, err := batch.Commit(ctx)
		if err != nil {
			return err
		}
	}
}

func idsArrayFromDoc(doc *firestore.DocumentSnapshot, field string) []string {
	m := doc.Data()
	a, ok := m[field].([]interface{})
	if !ok {
		return []string{}
	}

	var r []string
	if len(a) > 0 {
		r = make([]string, len(a))
		for indx, s := range a {
			r[indx] = s.(string)
		}
	}

	return r
}
