package repo

import (
	"context"
	"fmt"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/iterator"

	"gitlab.com/herd-project/herd-repo/types"
)

func (db *dbImpl) ApplyNotification(ctx context.Context, nType types.NotificationType, userID string, eventID string) ([]string, error) {
	switch nType {
	case types.NotificationUserLeaveEvent, types.NotificationUserJoinEvent, types.NotificationUserCloseEvent:
		attendees, err := db.applyAttendeesNotification(ctx, nType, userID, eventID)
		if err != nil {
			return nil, err
		}
		return attendees, db.addChatServiceMessage(ctx, nType, userID, eventID)
	case types.NotificationEventCreate:
		err := db.addChatServiceMessage(ctx, nType, userID, eventID)
		return nil, err
	default:
		return nil, types.ErrUnsupportedNotificationType
	}
}

func (db *dbImpl) GetNotifications(ctx context.Context, userID string) ([]types.Notification, error) {
	query := db.users.Doc(userID).Collection(subNotifications).OrderBy("timestamp", firestore.Desc).Limit(100)
	iter := query.Documents(ctx)

	result := make([]types.Notification, 0, 100)

	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, err
		}

		var p types.Notification
		err = doc.DataTo(&p)
		if err != nil {
			return nil, err
		}
		p.ID = doc.Ref.ID

		result = append(result, p)
	}
	return result, nil
}

func (db *dbImpl) DeleteNotification(ctx context.Context, userID, ID string) error {
	docRef := db.users.Doc(userID).Collection("notifications").Doc(ID)
	_, err := docRef.Delete(ctx)
	return err
}

// impl

func (db *dbImpl) applyAttendeesNotification(ctx context.Context, nType types.NotificationType, userID string, eventID string) ([]string, error) {
	// TODO: transaction
	event, err := db.GetEventFullInfo(ctx, eventID)
	if err != nil {
		return nil, err
	}

	affectedUsers := make([]string, 0, len(event.Attendees))

	batch := db.fs.Batch()

	data := map[string]interface{}{
		"timestamp": firestore.ServerTimestamp,
		"type":      nType,
		"user_id":   userID,
		"event_id":  eventID,
	}

	handledInstigator := false

	for _, attendee := range event.Attendees {
		docRef := db.users.Doc(attendee.UserID).Collection(subNotifications).NewDoc()
		batch.Set(docRef, data, firestore.MergeAll)
		affectedUsers = append(affectedUsers, attendee.UserID)
		if attendee.UserID == userID {
			handledInstigator = true
		}
	}

	docRef := db.users.Doc(event.Event.CreatorID).Collection(subNotifications).NewDoc()
	batch.Set(docRef, data, firestore.MergeAll)
	if event.Event.CreatorID == userID {
		handledInstigator = true
	}
	affectedUsers = append(affectedUsers, event.Event.CreatorID)

	if !handledInstigator {
		docRef := db.users.Doc(userID).Collection(subNotifications).NewDoc()
		batch.Set(docRef, data, firestore.MergeAll)
	}

	_, err = batch.Commit(ctx)
	return affectedUsers, err
}

var serviceChatMessages = map[types.NotificationType]string{
	types.NotificationEventCreate: "Run created",
	types.NotificationUserJoinEvent: "Good news! New dude is here! %s joined Run",
	types.NotificationUserLeaveEvent: "Uuups... %s left Run",
}

func (db *dbImpl) addChatServiceMessage(ctx context.Context, nType types.NotificationType, userID string, eventID string) error {
	chatMessage := types.IncomingChatMessage{
		EventID: eventID,
		IsBot:   true,
		LocalID: db.idGen.StringID(),
		Type:    types.ChatMessageTypeText,
	}

	addMessage := false

	switch nType {
	case types.NotificationEventCreate:
		chatMessage.Text = serviceChatMessages[types.NotificationEventCreate]
		addMessage = true
	case types.NotificationUserJoinEvent, types.NotificationUserLeaveEvent:
		profile, err := db.GetProfile(ctx, userID)
		if err != nil {
			return err
		}
		chatMessage.Text = fmt.Sprintf(serviceChatMessages[nType], profile.Name)
		addMessage = true
	}

	if addMessage {
		return db.ChatAddMessage(ctx, chatMessage)
	}

	return nil
}
