SHELL := /bin/zsh

test: export GCP_PROJECT_ID=herd-beta
test: export GOOGLE_APPLICATION_CREDENTIALS=../../secrets/service-key.json
test:
	go test -v ./...

lint:
	golangci-lint run
